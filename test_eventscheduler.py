#! /usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
import eventscheduler


class TestEventScheduler(unittest.TestCase):

    def test_init_without_parameters(self):
        """Test that __init__ without parameters
         set an empty array of events"""
        scheduler = eventscheduler.EventScheduler()
        self.assertEqual(len(scheduler.events), 0)

    def test_init_with_events(self):
        """Test that __init__ with events parameter set the good events"""
        events = [{"name": "name1", "teacher": "teacher1"}]
        expected = {"name1": {"name": "name1", "teacher": "teacher1"}}
        scheduler = eventscheduler.EventScheduler(events=events)
        self.assertEqual(expected, scheduler.events)

    def test_results_to_str(self):
        """Test basic conversion of results to string"""
        scheduler = eventscheduler.EventScheduler()
        scheduler._results = [[{"name": "nameA", "teacher": "teacherA"},
                               {"name": "nameB", "teacher": "teacherB"}]]
        expected = "1*****************************************\n"
        expected += "1 nameA (teacherA)\n"
        expected += "2 nameB (teacherB)\n"
        expected += "******************************************\n"
        str_results = scheduler.results_to_str()
        self.assertEqual(str_results, expected)

    def test_results_to_str_with_incompatibilities(self):
        """Test conversion of results to string with incompatibilities"""
        scheduler = eventscheduler.EventScheduler()
        scheduler._results = [[{"name": "nameA",
                                "teacher": "teacher1",
                                "minimal_distance": {"nameC": 1}},
                               {"name": "nameB", "teacher": "teacher2"},
                               {"name": "nameC",
                                "teacher": "teacher1",
                                "minimal_distance": {"nameA": 1}}]]
        expected = "1*****************************************\n"
        expected += "1 nameA (teacher1)\n"
        expected += " - nameC (1)\n"
        expected += "2 nameB (teacher2)\n"
        expected += "3 nameC (teacher1)\n"
        expected += " - nameA (1)\n"
        expected += "******************************************\n"
        result = scheduler.results_to_str(show_distances=True)
        self.assertEqual(result, expected)

    def test_results_to_str_with_limit(self):
        """Test conversion of results to string with limit of result number"""
        scheduler = eventscheduler.EventScheduler()

        firstresult = [{"name": "nameA", "teacher": "teacher1",
                        "minimal_distance": {"nameC": 1}},
                       {"name": "nameB", "teacher": "teacher2"},
                       {"name": "nameC", "teacher": "teacher1",
                        "minimal_distance": {"nameA": 1}}]
        secondresult = [{"name": "nameC", "teacher": "teacher1",
                         "minimal_distance": {"nameA": 1}},
                        {"name": "nameB", "teacher": "teacher2"},
                        {"name": "nameA", "teacher": "teacher1",
                         "minimal_distance": {"nameC": 1}}]
        scheduler._results = [firstresult, secondresult]

        expected1 = "- sample of 1 in 2-\n"
        expected1 += "1*****************************************\n"
        expected1 += "1 nameA (teacher1)\n"
        expected1 += " - nameC (1)\n"
        expected1 += "2 nameB (teacher2)\n"
        expected1 += "3 nameC (teacher1)\n"
        expected1 += " - nameA (1)\n"
        expected1 += "******************************************\n"
        expected2 = "- sample of 1 in 2-\n"
        expected2 += "1*****************************************\n"
        expected2 += "1 nameC (teacher1)\n"
        expected2 += " - nameA (1)\n"
        expected2 += "2 nameB (teacher2)\n"
        expected2 += "3 nameA (teacher1)\n"
        expected2 += " - nameC (1)\n"
        expected2 += "******************************************\n"
        result = scheduler.results_to_str(show_distances=True, result_number=1)
        self.assertTrue(result == expected1 or result == expected2)

    def test_check_events_empty(self):
        """Test check of empty events"""
        scheduler = eventscheduler.EventScheduler()
        self.assertFalse(scheduler.check_events())
        self.assertEqual(["Empty events"], scheduler.check_errors)

    def test_check_events_ok(self):
        """Test check of correct events"""
        events = [{"name": "nameA", "teacher": "teacher1",
                   "minimal_distance": {"nameC": 1}},
                  {"name": "nameB", "teacher": "teacher2"},
                  {"name": "nameC", "teacher": "teacher1",
                   "minimal_distance": {"nameA": 1}}]
        scheduler = eventscheduler.EventScheduler(events)
        self.assertTrue(scheduler.check_events())

    def test_check_events_forget_distance(self):
        """Test check of events with a forgotten distance"""
        events = [{"name": "nameA", "teacher": "teacher1",
                   "minimal_distance": {"nameC": 1}},
                  {"name": "nameB", "teacher": "teacher2"},
                  {"name": "nameC", "teacher": "teacher1"}]
        scheduler = eventscheduler.EventScheduler(events)
        self.assertFalse(scheduler.check_events())
        expected = ["No reciprocitity in distance 1@nameA@nameC"]
        self.assertEqual(expected, scheduler.check_errors)

    def test_get_available_events(self):
        """ Test get available events with only one event"""
        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB"}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()
        available = scheduler.get_available_events([eventA])
        expected = [eventB]
        self.assertEqual(expected, available)

    def test_get_available_events_with_allowed_indexes_ok(self):
        """ Test get available events with only one event
        and allowed_indexes compatible"""
        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB",
                  "allowed_indexes": [2]}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()
        available = scheduler.get_available_events([eventA])
        expected = [eventB]
        self.assertEqual(expected, available)

    def test_get_available_events_with_allowed_indexes_ko(self):
        """ Test get available events with only one event
        and allowed_indexes incompatible"""
        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB",
                  "allowed_indexes": [1]}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()
        available = scheduler.get_available_events([eventA])
        self.assertEqual(0, len(available))

    def test_get_available_events_with_forbidden_indexes_ok(self):
        """ Test get available events with only one event
        and forbidden_indexes compatible"""
        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB",
                  "forbidden_indexes": [1]}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()

        self.assertEqual(scheduler._allowed_indexes_by_name["nameA"], [1, 2])
        self.assertEqual(scheduler._allowed_indexes_by_name["nameB"], [2])
        available = scheduler.get_available_events([eventA])
        expected = [eventB]
        self.assertEqual(expected, available)

    def test_get_available_events_with_forbidden_indexes_ko(self):
        """ Test get available events with only one event
        and forbidden_indexes incompatible"""

        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB",
                  "forbidden_indexes": [2]}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()
        available = scheduler.get_available_events([eventA])
        self.assertEqual(0, len(available))

    def test_get_available_events_with_minimal_distance(self):
        """ Test get_available_events with two events including
        one with a minimal distance"""
        eventA = {"name": "nameA", "minimal_distance": {"nameC": 2}}
        eventB = {"name": "nameB"}
        eventC = {"name": "nameC", "minimal_distance": {"nameA": 2}}
        events = [eventA, eventB, eventC]
        scheduler = eventscheduler.EventScheduler(events)
        scheduler.check_events()
        available = scheduler.get_available_events([eventA])
        expected = [eventB]
        self.assertEqual(expected, available)

    def test_compute_basic(self):
        """Test the schedule with two compatible events"""
        eventA = {"name": "nameA", "teacher": "teacherA"}
        eventB = {"name": "nameB", "teacher": "teacherB"}
        events = [eventA, eventB]
        scheduler = eventscheduler.EventScheduler(events)
        expected = [[eventA, eventB], [eventB, eventA]]
        self.assertTrue(scheduler.compute())
        self.assertEqual(2, len(scheduler._results))
        self.assertEqual(expected, scheduler._results)

    def test_compute_with_forgotten_distance(self):
        """Test check of events with a forgotten distance"""
        events = [{"name": "nameA", "teacher": "teacher1",
                   "minimal_distance": {"nameC": 1}},
                  {"name": "nameB", "teacher": "teacher2"},
                  {"name": "nameC", "teacher": "teacher1"}]
        scheduler = eventscheduler.EventScheduler(events)
        self.assertFalse(scheduler.compute())

    def test_compute_with_allowed_indexes(self):
        """Test the schedule with two compatible events
        and one with allowed_indexes"""

        eventA = {"name": "nameA",
                  "teacher": "teacherA",
                  "allowed_indexes": [2]}
        eventB = {"name": "nameB", "teacher": "teacherB"}
        events = [eventA, eventB]

        scheduler = eventscheduler.EventScheduler(events)

        expected = [[eventB, eventA]]

        self.assertTrue(scheduler.compute())
        self.assertEqual(1, len(scheduler._results))
        self.assertEqual(expected, scheduler._results)

    def test_compute_with_forbidden_indexes(self):
        """Test the schedule with two compatible events
        and one with forbidden_indexes"""

        eventA = {"name": "nameA",
                  "teacher": "teacherA",
                  "forbidden_indexes": [1]}
        eventB = {"name": "nameB", "teacher": "teacherB"}
        events = [eventA, eventB]

        scheduler = eventscheduler.EventScheduler(events)

        expected = [[eventB, eventA]]

        self.assertTrue(scheduler.compute())
        self.assertEqual(1, len(scheduler._results))
        self.assertEqual(expected, scheduler._results)
