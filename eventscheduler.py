#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import io
import json
import random


class EventScheduler():

    def __init__(self, events=[]):
        self._results = None
        self.set_events(events)
        self.check_errors = []

    def set_events(self, events):
        self.events = {}
        for event in events:
            self.events[event.get("name")] = event

    def one_result_to_str(self, result, show_distances=False):
        returnstr = "*****************************************\n"
        i = 1
        for event in result:
            returnstr += str(i) + " " + event.get("name")
            returnstr += " (" + str(event.get("teacher")) + ")\n"
            if show_distances:
                minimal_distance = event.get("minimal_distance", {})
                if 0 != len(minimal_distance):
                    for k, v in minimal_distance.items():
                        returnstr += " - " + k + " (" + str(v) + ")"
                    returnstr += "\n"
            i += 1
        returnstr += "******************************************\n"
        return returnstr

    def results_to_str(self, show_distances=False, result_number=None):
        """Return the string of computed results"""
        returnstr = ""
        results = self._results
        results_len = len(self._results)
        if result_number is not None and result_number < len(self._results):
            indices = random.sample(range(len(results)), result_number)
            results = [results[i] for i in sorted(indices)]
            returnstr = "- sample of " + str(result_number) + " in "
            returnstr += str(results_len) + "-\n"
        result_count = 0
        for result in results:
            result_count += 1
            returnstr += str(result_count)
            returnstr += self.one_result_to_str(result, show_distances)

        return returnstr

    def check_events(self):
        """ Check the correctness of events array"""
        checked = False
        self.check_errors = ["Empty events"]
        self.minimal_distance = {}
        self._allowed_indexes_by_name = {}
        self.eventcount = len(self.events)
        default_allowed_indexes = range(1, len(self.events)+1)
        distancecheck = {}

        for name, event in self.events.items():
            self.check_errors = []
            checked = True
            event_minimal_distance = event.get("minimal_distance", {})
            for farname, distance in event_minimal_distance.items():
                key = '@'.join(sorted([name, farname]))
                self.minimal_distance[key] = int(distance)
                checkkey = '@'.join(sorted([str(distance), name, farname]))
                checkcount = distancecheck.get(checkkey, 0)
                checkcount = checkcount + 1
                distancecheck[checkkey] = checkcount

            allowed_indexes = event.get("allowed_indexes")
            if allowed_indexes is None:
                allowed_indexes = list(default_allowed_indexes)

            forbidden_indexes = event.get("forbidden_indexes")
            if forbidden_indexes is not None:
                generator = (i for i in forbidden_indexes
                             if i in allowed_indexes)
                for i in generator:
                    allowed_indexes.remove(i)
            self._allowed_indexes_by_name[name] = allowed_indexes

        for key, value in distancecheck.items():
            if 2 != value:
                self.check_errors.append("No reciprocitity in distance " + key)
                checked = False

        return checked

    def _event_compatible_with_index(self, event, index):
        """Return true if event is compatible with the index"""
        allowed_indexes = self._allowed_indexes_by_name.get(event.get("name"))
        if index in allowed_indexes:
            return True
        else:
            return False

    def get_available_events(self, scheduled_events):
        """Return the list of compatible events with scheduled_events"""
        available_events = []
        next_index = len(scheduled_events) + 1

        for name, event in self.events.items():
            found = False
            distanceok = True
            indexok = False
            for i, scheduled_event in enumerate(scheduled_events):
                scheduled_name = scheduled_event.get("name")
                if name == scheduled_name:
                    found = True
                    break
                key = '@'.join(sorted([name, scheduled_name]))
                minimal_distance = self.minimal_distance.get(key, -1)
                distance = next_index - i
                if minimal_distance >= distance:
                    distanceok = False
                    break
            if found is False:
                indexok = self._event_compatible_with_index(event,
                                                            next_index)
                if distanceok and indexok:
                    available_events.append(event)
        return available_events

    def _compute_iter(self, current_event, show_progress, scheduled_events):
        """Iter the scheduled events compatible with constraints
        and to put the results in _results"""
        return_value = False

        future_scheduled_events = list(scheduled_events)
        if current_event is not None:
            future_scheduled_events.append(current_event)
        available_events = self.get_available_events(future_scheduled_events)
        if future_scheduled_events is not None:
            if len(future_scheduled_events) == self.eventcount:
                self._results.append(future_scheduled_events)
                if show_progress:
                    self._results_ok += 1
                    if 0 == self._results_ok % 1000000:
                        print("OK :", self._results_ok)
                        print(self.one_result_to_str(future_scheduled_events,
                                                     True))
                return True
        no_more_events = 0 == len(available_events)
        unfinished_events = len(future_scheduled_events) < self.eventcount
        if no_more_events and unfinished_events:
            if show_progress:
                self._results_ko += 1
                if 0 == self._results_ko % 1000000:
                    print("KO :", self._results_ko)
            return False

        for child in available_events:
            if self._compute_iter(child,
                                  show_progress,
                                  future_scheduled_events):
                return_value = True
        return return_value

    def compute(self, show_progress=False):
        """Compute the scheduled events compatible with constraints
        and to put results in _results"""
        return_value = False
        if self.check_events() is False:
            return False

        self._results = []
        self._results_ok = 0
        self._results_ko = 0
        generator = (event for name, event in self.events.items()
                     if self._event_compatible_with_index(event, 1))
        for event in generator:
            if self._compute_iter(None, show_progress,
                                  scheduled_events=[event]):
                return_value = True
        return return_value


def main(events, outfilename):
    scheduler = EventScheduler(events)
    if scheduler.check_events():
        scheduler.compute(True)
        print(len(scheduler._results))
        with io.open(outfilename, "w", encoding="utf8") as fout:
            fout.write(scheduler.results_to_str(result_number=100,
                                                show_distances=False))
    else:
        for error in scheduler.check_errors:
            print(error)


if __name__ == '__main__':
    infilename = sys.argv[1]
    events = json.load(open(infilename))
    outfilename = infilename + ".txt"
    main(events, outfilename)
