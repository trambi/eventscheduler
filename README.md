# Event Scheduler

This a small resolver to determine schedule of events based on constraints described on JSON configuration file.

## How to use

The call of this command
```bash
python3 eventscheduler example.json
```
will create a "example.json.txt" that describe the eight (8) possible schedule.

## How to launch test

Call the command 
```bash
pytest --cov=eventscheduler .
```.
